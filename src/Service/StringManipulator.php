<?php

namespace Drupal\char_count_formatter\Service;

use Drupal\Core\Config\ConfigFactory;

/**
 * Class StringManipulator.
 *
 * @package Drupal\char_count_formatter\Service
 */
class StringManipulator {
  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * StringManipulator constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactoryService
   *   The Drupal Config Factory service.
   * 
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * Converts a String to Slug Format
   * 
   * @param String
   *   String to be converted
   * @return String
   *   New string with appended character count
   */
  public function charCount($str) {
    $config = $this->configFactory->get('char_count_formatter.settings');
    $wrap = $config->get('wrap_characters');
    $count = strlen($str);

    switch ($wrap) {
      case 'parenthesis':
        $str .= " ({$count})";
        break;
      case 'brackets':
        $str .= " [{$count}]";
        break;
      case 'curly':
        $str .= " {{$count}}";
        break;
    }

    return $str;
  }
}
