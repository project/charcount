<?php

namespace Drupal\char_count_formatter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class CharCountConfigForm extends ConfigFormBase {

  /**
  
   * {@inheritdoc}
  
   */

  public function getFormId() {

    return 'formatter_config_form';
  }

  /**
  
   * {@inheritdoc}
  
   */

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('char_count_formatter.settings');

    $form['wrap_characters'] = [
      '#type' => 'select',
      '#title' => $this->t('Wrap Characters'),
      '#required' => true,
      '#options' => [
        'parenthesis' => 'Parenthesis, ()',
        'brackets' => 'Brackets, []',
        'curly' => 'Curly Braces, {}',
      ],
      '#default_value' => $config->get('wrap_characters'),
    ];

    return $form;
  }

  /**
  
   * {@inheritdoc}
  
   */

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('char_count_formatter.settings');
    $config->set('wrap_characters', $form_state->getValue('wrap_characters'));
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
  
   * {@inheritdoc}
  
   */

  protected function getEditableConfigNames() {
    return ['char_count_formatter.settings'];
  }
}
